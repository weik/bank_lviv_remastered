import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final String firstCardNum;
  final String secondCardNum;
  final String thirdCardNum;
  final String fourthCardNum;
  final String monthExpired;
  final String yearExpired;

  MyCard({
    this.firstCardNum,
    this.secondCardNum,
    this.thirdCardNum,
    this.fourthCardNum,
    this.yearExpired,
    this.monthExpired,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child:Card(
       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
       color: Colors.blueGrey[800],
       child: Padding(
         padding: EdgeInsets.symmetric(
           horizontal: 20.0,
            vertical: 30.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.credit_card,
                  color: Colors.blueGrey,
                  size: 40,
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  'BANK LVIV',
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '$firstCardNum\t $secondCardNum\t $thirdCardNum\t $fourthCardNum',
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.blueGrey,
                  ),
                ),
                Icon(
                  Icons.nfc,
                  color: Colors.yellowAccent,
                  size: 35.0,
                )
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  '$monthExpired\/$yearExpired',
                  style: TextStyle(
                    fontSize: 17.0,
                    color: Colors.blueGrey,
                  ),
                ),
                SizedBox(
                  width: 120,
                ),
                Icon(
                  Icons.credit_card,
                  size: 35,
                  color: Colors.blueGrey,
                )
              ],
            ),
          ],
        ),
      ),),
    );
  }
}