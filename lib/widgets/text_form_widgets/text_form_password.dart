import 'package:flutter/material.dart';

import '../../data/log_in_data.dart';

class TextFormPassword extends StatelessWidget {
  final focusPassword = FocusNode();

  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Password',
      ),
      keyboardType: TextInputType.visiblePassword,
      cursorColor: Colors.white,
      obscureText: true,
      focusNode: focusPassword,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Your password is required';
        }
        return null;
      },
      onSaved: (String value) {
        FormData().formData['password'] = value;
      },
    );
  }
}
