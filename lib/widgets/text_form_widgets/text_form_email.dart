import 'package:flutter/material.dart';

import '../../data/log_in_data.dart';
import 'text_form_password.dart';

class TextFormEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Login',
      ),
      maxLength: 10,
      cursorColor: Colors.white,
      keyboardType: TextInputType.number,
      onFieldSubmitted: (v) {
        FocusScope.of(context).requestFocus(TextFormPassword().focusPassword);
      },
      validator: (String value) {
        if (value.isEmpty) {
          return 'Your login is required';
        }
        if (RegExp(
                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
          return 'Please enter a valid login number';
        }
        return null;
      },
      onSaved: (String value) {
        FormData().formData['email'] = value;
      },
    );
  }
}
