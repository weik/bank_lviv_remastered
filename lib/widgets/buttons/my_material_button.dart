import 'package:flutter/material.dart';

class MyMaterialButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final Color color;

  MyMaterialButton({
    @required this.onPressed,
    this.text,
    this.color,
  });

  Widget build(BuildContext context) {
    return MaterialButton(
      color: color,
      minWidth: 220.0,
      padding: EdgeInsets.all(10),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(fontSize: 23, fontWeight: FontWeight.w600),
      ),
    );
  }
}
