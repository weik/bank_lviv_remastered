import 'package:flutter/material.dart';

class MyListTile extends StatelessWidget {
  final Widget leading;
  final String title;
  final String subtitle;
  final Function onTap;
  final Widget trailing;

  MyListTile(
      {this.leading, this.title, this.subtitle, this.onTap, this.trailing});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading,
      title: Text(
        title,
        style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        subtitle,
        style: TextStyle(
          fontSize: 15.0,
        ),
      ),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: onTap,
    );
  }
}
