import 'dart:ui';

import 'package:flutter/material.dart';

import '../home_screen/home_page_screen.dart';
import '../../data/log_in_data.dart';
import '../../widgets/buttons/my_material_button.dart';
import '../../widgets/text_form_widgets/text_form_email.dart';
import '../../widgets/text_form_widgets/text_form_password.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'Login_Screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm() {
    print('Submitting form');
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save(); //onSaved is called!
      print(FormData().formData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/background.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 30.0,
                ),
                ShaderMask(
                  shaderCallback: (Rect bounds) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      tileMode: TileMode.mirror,
                      colors: <Color>[
                        Colors.pink.shade700,
                        Colors.pink.shade100,
                      ],
                    ).createShader(bounds);
                  },
                  child: Text(
                    'BANK\nLVIV',
                    style: TextStyle(
                      fontSize: 60.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                ShaderMask(
                  shaderCallback: (Rect bounds) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      tileMode: TileMode.mirror,
                      colors: <Color>[
                        Colors.pink.shade100,
                        Colors.pink.shade700,
                      ], //, Color(0xff8921aa)],
                    ).createShader(bounds);
                  },
                  child: Text(
                    'The Bank of the\nnew opportunities',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                SizedBox(
                  height: 150.0,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormEmail(),
                      TextFormPassword(),
                      SizedBox(
                        height: 100.0,
                      ),
                      MyMaterialButton(
                        text: 'Submit',
                        color: Colors.pink[800],
                        onPressed: () {
                          _submitForm();
                          Navigator.pushNamed(context, HomePage.id);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
