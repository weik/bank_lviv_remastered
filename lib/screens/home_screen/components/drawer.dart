import '../../../constants.dart';
import '../../../widgets/list_tile/drawer_listtile.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            margin: EdgeInsets.all(8.0),
            child: Text(
              'BANK LVIV',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 60.0,
                fontWeight: FontWeight.bold,
                foreground: Paint()..shader = linearGradientLabel,
              ),
            ),
            decoration: BoxDecoration(
                color: Colors.pink[400],
                //gradient: drawerHeaderGradient,
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
          ),
          SizedBox(
            height: 55.0,
          ),
          MyListTile(
            title: 'hey',
            subtitle: 'hay',
          ),
          SizedBox(
            height: 15.0,
          ),
          MyListTile(
            title: 'hewy',
            subtitle: 'sdasd',
          ),
        ],
      ),
    );
  }
}
