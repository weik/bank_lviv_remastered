import 'dart:io' show Platform;

import 'package:bank_lviv_remastered/widgets/card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBody extends StatefulWidget {
  @override
  _MyBodyState createState() => _MyBodyState();
}

class _MyBodyState extends State<MyBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/background.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                MyCard(
                  firstCardNum: '4444',
                  secondCardNum: '4444',
                  thirdCardNum: '4444',
                  fourthCardNum: '4444',
                  monthExpired: '01',
                  yearExpired: '02',
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Align(
              alignment: Alignment.topRight,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.pink[800],
                child: Text(
                  'delete credit card',
                  style: TextStyle(
                    color: Colors.white70,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                onPressed: () async {
                  Platform.isIOS
                      ? showCupertinoDialog(
                          context: context,
                          builder: (_) => CupertinoAlertDialog(
                            title: Text('Alert Dialog'),
                            content: Text(
                                'Well you will delete literally nothing. Are you sure to continue?'),
                            actions: <Widget>[
                              CupertinoDialogAction(
                                child: Text('no'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              CupertinoDialogAction(
                                child: Text('yes'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          ),
                        )
                      : showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            title: Text('Alert Dialog'),
                            content: Text(
                                'Well you will delete literally nothing. Are you sure to continue?'),
                            actions: <Widget>[
                              FlatButton(
                                child: Text('no'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              FlatButton(
                                child: Text('yes'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                            backgroundColor: Colors.blueGrey,
                          ),
                        );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
