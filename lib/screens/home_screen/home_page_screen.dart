import '../home_screen/components/body.dart';
import '../home_screen/components/drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static const String id = 'HomePage_Screen';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home Page',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                //Implement logout functionality
                Navigator.pop(context);
              }),
        ],
      ),
      drawer: MyDrawer(),
      body: MyBody(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.pink[800],
        child: Icon(
          Icons.add,
          color: Colors.white70,
          size: 40,
        ),
        onPressed: () {},
      ),
    );
  }
}
