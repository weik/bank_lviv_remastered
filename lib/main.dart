import 'package:bank_lviv_remastered/screens/home_screen/home_page_screen.dart';
import 'package:bank_lviv_remastered/screens/login_screen/login_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData.dark().copyWith(
        appBarTheme: AppBarTheme(color: Colors.pink[800]),
        accentColor: Colors.white,
        buttonColor: Colors.pink[800],
      ),
      initialRoute: LoginScreen.id,
      routes: {
        LoginScreen.id: (context) => LoginScreen(),
        HomePage.id: (context) => HomePage(),
      },
    );
  }
}
