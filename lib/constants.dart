import 'package:flutter/material.dart';

const buttonTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
);

const bigBankLabelStyle = TextStyle(
  fontSize: 60.0,
  fontWeight: FontWeight.bold,
);

const smallBankLabelStyle = TextStyle(
  color: Colors.white,
  fontSize: 20.0,
);

final Shader linearGradientLabel = LinearGradient(
  colors: <Color>[Colors.white, Colors.pink[800]],
).createShader(Rect.fromLTWH(0.0, 50.0, 200.0, 70.0));

final Gradient drawerHeaderGradient = LinearGradient(
    colors: <Color>[Colors.black, Colors.white],
    begin: FractionalOffset(1, 1),
    end: FractionalOffset(0, 1),
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp);
